package com.ultralesson.eventplanner;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.ultralesson.eventplanner.model.Attendee;

public class AttendeeTest {
    private Attendee attendee;

    @BeforeMethod
    public void setUp() {
        attendee = new Attendee(1, "John Doe", "john.doe@example.com");
    }

    @AfterMethod
    public void tearDown() {
        attendee = null;
    }

    @Test
    public void testAttendeeProperties() {
        Attendee attendee = new Attendee(1, "John Doe", "john.doe@example.com");
        // Add TestNG assertions here to validate attendee properties
        Assert.assertEquals(attendee.getId(), 1);
        Assert.assertEquals(attendee.getName(), "John Doe");
        Assert.assertEquals(attendee.getEmail(), "john.doe@example.com");
    }

//    @Test(expectedExceptions = IllegalArgumentException.class)
// public void testAttendeeEmailValidation() {
//     // The email "invalid_email" is intentionally invalid to trigger the IllegalArgumentException
//     Attendee attendee = new Attendee(1, "John Doe", "invalid_email");
// }

}
