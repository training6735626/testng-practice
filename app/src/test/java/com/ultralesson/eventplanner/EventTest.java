package com.ultralesson.eventplanner;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.ultralesson.eventplanner.model.Event;
import com.ultralesson.eventplanner.model.Venue;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class EventTest {
    @BeforeClass
    public void setUpClass() {
        System.out.println("Setting up resources for EventTest class...");
    }

    @AfterClass
    public void tearDownClass() {
        System.out.println("Releasing resources for EventTest class...");
    }

    @Test
    public void testEventCreation() {
        Venue venue = new Venue(1, "Test Venue", "123 Test Street", 200);
        Event event = new Event(1, "Test Event", "This is a test event.", venue);
        Assert.assertNotNull(event, "Event creation failed; event instance is null.");
    }

    @Test
    public void testEventProperties() {
        Venue venue = new Venue(1, "Test Venue", "123 Test Street", 200);
        Event event = new Event(1, "Test Event", "This is a test event.", venue);

        Assert.assertEquals(event.getId(), 1, "Event ID mismatch");
        Assert.assertEquals(event.getName(), "Test Event", "Event name mismatch");
        Assert.assertEquals(event.getDescription(), "This is a test event.", "Event description mismatch");
        Assert.assertEquals(event.getVenue(), venue, "Event venue mismatch");
    }

}
