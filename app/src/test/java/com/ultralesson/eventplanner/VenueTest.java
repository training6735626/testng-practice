package com.ultralesson.eventplanner;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ultralesson.eventplanner.model.Venue;

@Test
public class VenueTest {
    @Test
    public void TestVenueCreation() {
        Venue venue = new Venue(1, "", "This is a test venue.", 100);
        Assert.assertNotNull(venue, "Venue instance should not be null.");
    }
    @Test
    public void testVenueProperties() {
        int id = 1;
        String name = "Test ";
        String address = "This is a test venue.";
        int capacity = 100;
    
        Venue venue = new Venue(id, name, address, capacity);
    
        Assert.assertEquals(venue.getId(), id, "Venue ID doesn't match.");
        Assert.assertEquals(venue.getName(), name, "Venue name doesn't match.");
        Assert.assertEquals(venue.getAddress(), address, "Venue address doesn't match.");
        Assert.assertEquals(venue.getCapacity(), capacity, "Venue capacity doesn't match.");
    }
}
